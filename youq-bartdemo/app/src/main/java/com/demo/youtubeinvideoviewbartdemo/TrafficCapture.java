package com.demo.youtubeinvideoviewbartdemo;

/**
 * Created by Bart on 15.11.2016..
 */

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TrafficCapture extends IntentService {

    /**
     * Class constructor
     */
    public TrafficCapture() {
        super("testService");
    }

    /**
     * @param workIntent Method onHandleIntent captures network traffic using
     *                   Android command line
     *                   <p/>
     */
    @Override
    protected void onHandleIntent(Intent workIntent) {
        Bundle extras = workIntent.getExtras();
        String time = extras.get("time").toString();
        try {
            Process capturingProcess = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(capturingProcess.getOutputStream());
            String cmd = "tcpdump -s0 -w /sdcard/tmp/" + time + "_traffic.pcap";
            //String cmd = "tcpdump -s 96 -w /sdcard/tmp/" + time + "_traffic.pcap 'tcp'";
            os.writeBytes(cmd);
            os.flush();

            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}