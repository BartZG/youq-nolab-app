package com.demo.youtubeinvideoviewbartdemo;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class EndActivity extends Activity {
    Integer numOfVid;
    Integer numOfStallingE;
    Float totalVideoDuration;
    Float experimentDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            numOfVid = extras.getInt("numOfVid");
            numOfStallingE = extras.getInt("numOfStallingE");
            totalVideoDuration = extras.getFloat("totalVideoDuration");
            experimentDuration = extras.getFloat("experimentDuration");
        }
        setContentView(R.layout.activity_end);
        TextView vidPlayed = (TextView) findViewById(R.id.textView9);
        vidPlayed.setText("Videos played: " + numOfVid);
        TextView stalling = (TextView) findViewById(R.id.textView10);
        stalling.setText("Number of stalling events: " + (numOfStallingE));
        TextView vidDuration = (TextView) findViewById(R.id.textView12);
        vidDuration.setText("Total video duration: " + totalVideoDuration);
        TextView expDuration = (TextView) findViewById(R.id.textView13);
        expDuration.setText("Experiment duration: " + experimentDuration);
    }

    public void uploadAndClose(View view){
        boolean status = upload();
        if (status) {
            removeTmpFiles();
            finish();
        } else {
            Toast.makeText(this, "Failed to upload files!", Toast.LENGTH_SHORT).show();
        }
    }


    public void uploadAndNew(View view){
        boolean status = upload();
        if (status){
            removeTmpFiles();
            Intent intent = new Intent(this, YoutubeActivity.class);
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(this, "Failed to upload files!", Toast.LENGTH_SHORT).show();
        }
    }



    public boolean upload(){
        FTPClient mFtpClient = new FTPClient();
        boolean status = false;
        try {
            mFtpClient.setConnectTimeout(10 * 1000);
            mFtpClient.connect("161.53.19.92",21);
            status = mFtpClient.login("youq", "youq");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (status) {
            File[] files = new File("/sdcard/tmp/").listFiles();
            for (File file : files) {
                if (file.isFile()) {
                    String filename = file.getName();
                    try {
                        FileInputStream srcFileStream = new FileInputStream("/sdcard/tmp/" + filename);
                        status = mFtpClient.storeFile(filename,
                                srcFileStream);
                        srcFileStream.close();
                        if (!status) break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return status;
    }

    public void removeTmpFiles() {
        File[] files = new File("/sdcard/tmp/").listFiles();
        for (File file : files) {
            if (file.isFile()) {
                file.delete();
            }
        }
    }

    public void close(View view){
        removeTmpFiles();
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }

}
