package com.demo.youtubeinvideoviewbartdemo;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import android.telephony.CellInfo;
import android.telephony.CellInfoLte;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.lang.*;
import java.security.KeyStore;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by irena01 on 7.11.2015..
 */
public class WebAppInterface {

    public interface Listener {
        void onLastVideoPlayed();
    }

    Context mContext;
    FileOutputStream outputStreamE;
    FileOutputStream outputStreamB;
    FileOutputStream outputStreamU;

    Integer numOfPlayedVideos;
    Listener mListener;
    Integer numOfStallingE;
    Float totalVideoDuration;
    long firstEventTime;
    long lastEventTime;
    String currentVideo;
    Set playedVideos;
    WifiManager wifiManager;
    TelephonyManager telephonyManager;
    /** Instantiate the interface and set the context */
    WebAppInterface(Context c, long expStartTime) {
        mContext = c;
        currentVideo = new String();
        playedVideos = new HashSet<>();
        this.wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        this.telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        this.numOfPlayedVideos = 0;
        this.numOfStallingE = 0;
        this.totalVideoDuration = (float) 0;
        this.firstEventTime = 0;
        this.lastEventTime = 0;

        long filename = System.currentTimeMillis();
        String filenameE = "events" + expStartTime + ".csv";
        String filenameB = "buffer" + expStartTime + ".csv";
        String filenameU = "urls" + expStartTime + ".csv";
        mListener = null;
        try {
            System.out.println("**********************************" + Environment.getExternalStorageDirectory().getPath());
            outputStreamE = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/tmp", filenameE));
            outputStreamB = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/tmp", filenameB));
            outputStreamU = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/tmp", filenameU));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /** Show a toast from the web page */

    @JavascriptInterface
    public void handleEvent(String event) throws IOException {
        boolean lastVideo = false;
        int data = 0;
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        Toast.makeText(mContext, event.split(",")[3], Toast.LENGTH_SHORT).show();
        try {
            final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            for (final CellInfo info : tm.getAllCellInfo()) {
                if (info instanceof CellInfoGsm) {
                    data = ((CellInfoGsm) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoWcdma) {
                    data = ((CellInfoWcdma) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoLte) {
                    data = ((CellInfoLte) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else {
                    throw new Exception("Unknown type of cell signal!");
                }
            }
        } catch (Exception e) {

        }

        // Toast.makeText(mContext, numOfPlayedVideos.toString(), Toast.LENGTH_SHORT).show();
        if (firstEventTime == 0) firstEventTime=Long.valueOf(event.split(",")[1]);
        lastEventTime = Long.valueOf(event.split(",")[1]);
        if (event.split(",")[3].equals("Ended")){
            if(event.split(",")[4].equals("1"))
                lastVideo = true;
            event = event.substring(0,event.length()-2);
            numOfPlayedVideos++;
            totalVideoDuration += Float.valueOf(event.split(",")[2]);
        }
        if (event.split(",")[3].equals("Buffering") && !event.split(",")[2].equals("0.00")){
            numOfStallingE++;
        }
        currentVideo = event.split(",")[0];
        if (!playedVideos.contains(currentVideo)){
            //Socket s = new Socket("10.19.128.214", 12345);
            playedVideos.add(currentVideo);
        }
        try {
            outputStreamE.write((event + "," + linkSpeed + "," + data + "\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (lastVideo){

            mListener.onLastVideoPlayed();
        }
    }
    @JavascriptInterface
    public void handleBuffer(String bufferState) {
        int data = 0;
        //Network type variable
        String netType = "";

        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        System.out.println("*******************************************");
        System.out.println(linkSpeed);
        try {
            final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            for (final CellInfo info : tm.getAllCellInfo()) {
                if (info instanceof CellInfoGsm) {
                    data = ((CellInfoGsm) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoWcdma) {
                    data = ((CellInfoWcdma) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoLte) {
                    data = ((CellInfoLte) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else {
                    throw new Exception("Unknown type of cell signal!");
                }
            }
        } catch (Exception e) {

        }
        //Network type:
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    netType = "GPRS";
                    break;
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    netType = "EDGE";
                    break;
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    netType = "CDMA";
                    break;
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    netType = "1xRTT";
                    break;
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    netType = "IDEN";
                    break;
                    //return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    netType = "UMTS";
                    break;
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    netType = "EVDO_rev_0";
                    break;
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    netType = "EVDO_rev_A";
                    break;
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    netType = "HSDPA";
                    break;
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    netType = "HSUPA";
                    break;
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    netType = "HSPA";
                    break;
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    netType = "EVDO_rev_B";
                    break;
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    netType = "EHRPD";
                    break;
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    netType = "HSPAP";
                    break;
                    //return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    netType = "4G";
                    break;
                    //return "4G";
                default:
                    netType = "Unknown";
                    break;
                    //return "Unknown";
            }
        } catch (Exception e) {
            //
        }

        //Toast.makeText(mContext, bufferState, Toast.LENGTH_SHORT).show();
        try {
            outputStreamB.write((bufferState+","+linkSpeed+","+data+","+netType+"\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logResourceURL(String url) {
        try {
            long currentTime = System.currentTimeMillis();
            outputStreamU.write(( currentTime + "|" + currentVideo + "|" + url + "\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void finish() throws IOException {
        outputStreamE.close();
        outputStreamB.close();
        outputStreamU.close();
    }

    public Integer getnumOfStallingE() {
        return numOfStallingE;
    }

    public Float getTotalVideoDuration() {
        return totalVideoDuration;
    }

    public Float getExperimentDuration(){
        return (float)(lastEventTime-firstEventTime)/1000;
    }

    public Integer getNumOfPlayedVideos() {
        return numOfPlayedVideos;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

}